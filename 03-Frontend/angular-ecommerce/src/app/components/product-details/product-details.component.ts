import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/common/product';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from 'src/app/common/cart-item';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  selectedProduct: Product = new Product();

  constructor(private productService: ProductService,
              private cartService: CartService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.hundleProductDetails();
    });
  }

  hundleProductDetails() {
    // get the selected product id and convert to a number 
    const theProductId: number = +this.route.snapshot.paramMap.get('id');
    this.productService.getProduct(theProductId).subscribe(
      data => {
        this.selectedProduct = data;
      });
  }

  addToCart() {
    console.log(`Adding to cart: ${this.selectedProduct.name}, ${this.selectedProduct.unitPrice}`);

    const theCartItem = new CartItem(this.selectedProduct);
    this.cartService.addToCart(theCartItem);
  }
}